﻿#region Using

using System;
using System.Text.RegularExpressions;

#endregion

namespace ChangeRevisionNumber
{
    public class VersionExt
    {
        private int _major;
        private int _minor;
        private int _build;
        private int _revision;

        #region Поля

        public int Major
        {
            get { return this._major; }
            set
            {
                int num;
                if ( !int.TryParse( value.ToString(), out num ) )
                {
                    throw new Exception( "Incorect input value!" );
                }
                this._major = value;
            }
        }

        public int Minor
        {
            get { return this._minor; }
            set
            {
                int num;
                if ( !int.TryParse( value.ToString(), out num ) )
                {
                    throw new Exception( "Incorect input value!" );
                }
                this._minor = value;
            }
        }

        public int Build
        {
            get { return this._build; }
            set
            {
                int num;
                if ( !int.TryParse( value.ToString(), out num ) )
                {
                    throw new Exception( "Incorect input value!" );
                }
                this._build = value;
            }
        }

        public int Revision
        {
            get { return this._revision; }
            set
            {
                int num;
                if ( !int.TryParse( value.ToString(), out num ) )
                {
                    throw new Exception( "Incorect input value!" );
                }
                this._revision = value;
            }
        }

        #endregion

        public VersionExt( string version )
        {
            var regex = new Regex( "([0-9]+)\\.([0-9]+)(\\.([0-9]+)(\\.([0-9]+))?)?" );
            var match = regex.Match( version );
            if ( !match.Success )
            {
                throw new Exception( "Incorect input string!" );
            }
            this._major = int.Parse( match.Groups[ 1 ].ToString() );
            this._minor = int.Parse( match.Groups[ 2 ].ToString() );
            if ( !string.IsNullOrEmpty( match.Groups[ 4 ].ToString() ) )
            {
                this._build = int.Parse( match.Groups[ 4 ].ToString() );
            }
            if ( !string.IsNullOrEmpty( match.Groups[ 6 ].ToString() ) )
            {
                this._revision = int.Parse( match.Groups[ 6 ].ToString() );
            }
        }

        #region Operators

        public static bool operator ==( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }

            return v1.Equals( v2 );
        }

        public static bool operator !=( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }

            return !v1.Equals( v2 );
        }

        public static bool operator >( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }
            int v1Num = int.Parse( v1.ToString().Replace( ".", "" ) );
            int v2Num = int.Parse( v2.ToString().Replace( ".", "" ) );

            return v1Num > v2Num;
        }

        public static bool operator <( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }
            int v1Num = int.Parse( v1.ToString().Replace( ".", "" ) );
            int v2Num = int.Parse( v2.ToString().Replace( ".", "" ) );

            return v1Num < v2Num;
        }

        public static bool operator >=( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }

            return ( v1 > v2 || v1 == v2 );
        }

        public static bool operator <=( VersionExt v1, VersionExt v2 )
        {
            // если оба null или указывают на один объект
            if ( ReferenceEquals( v1, v2 ) )
            {
                return true;
            }

            // если один из них null
            if ( ( (object) v1 == null ) || ( (object) v2 == null ) )
            {
                return false;
            }

            return ( v1 < v2 || v1 == v2 );
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Major + "." + this.Minor + "." + this.Build + "." + this.Revision;
        }

        public override bool Equals( object obj )
        {
            var ve = obj as VersionExt;
            // если не может быть представлен как VersionExt
            if ( ve == null )
            {
                return false;
            }
            return this.Equals( ve );
        }

        public override int GetHashCode()
        {
            return this.Major.GetHashCode() ^ this.Minor.GetHashCode() ^ this.Build.GetHashCode() ^
                   this.Revision.GetHashCode();
        }

        public bool Equals( VersionExt obj )
        {
            if ( obj == null )
            {
                return false;
            }
            return this.Major.Equals( obj.Major ) && this.Minor.Equals( obj.Minor ) && this.Build.Equals( obj.Build ) &&
                   this.Revision.Equals( obj.Revision );
        }

        #endregion
    }
}
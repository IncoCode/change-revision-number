﻿#region Using

using System;
using System.IO;
using System.Text.RegularExpressions;

#endregion

namespace ChangeRevisionNumber
{
    internal class Program
    {
        private static void Main( string[] args )
        {
            if ( args.Length == 0 )
            {
                Console.WriteLine( "Specify a list of files!" );
                return;
            }
            foreach ( string fileName in args )
            {
                if ( !File.Exists( fileName ) )
                {
                    Console.WriteLine( "File not found!" );
                    continue;
                }
                var regex =
                    new Regex( "\\[assembly: AssemblyFileVersion\\( \"([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)\" \\)\\]" );
                string content = File.ReadAllText( fileName );
                Match match = regex.Match( content );
                if ( !match.Success )
                {
                    Console.WriteLine( "No matches found!" );
                    continue;
                }
                string versionStr = match.Groups[ 1 ].ToString();
                VersionExt version;
                try
                {
                    version = new VersionExt( versionStr );
                }
                catch ( Exception ex )
                {
                    Console.WriteLine( "Parsing error: " + ex.Message );
                    continue;
                }
                version.Revision++;
                string resultStr = "[assembly: AssemblyFileVersion( \"" + version + "\" )]";
                string result = regex.Replace( content, resultStr );
                try
                {
                    File.WriteAllText( fileName, result );
                    Console.WriteLine( "OK" );
                }
                catch
                {
                    Console.WriteLine( "Write result error!" );
                }
            }
        }
    }
}
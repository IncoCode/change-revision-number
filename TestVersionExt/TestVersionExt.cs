﻿#region Using

using ChangeRevisionNumber;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace TestVersionExt
{
    [TestClass]
    public class TestVersionExt
    {
        #region Parse Test

        [TestMethod]
        public void FourNum_Test()
        {
            var version = new VersionExt( "1.0.2.8" );
            bool res = ( version.Major == 1 && version.Minor == 0 && version.Build == 2 && version.Revision == 8 );
            Assert.IsTrue( res );
        }

        [TestMethod]
        public void ThirdNum_Test()
        {
            var version = new VersionExt( "1.0.2." );
            bool res = ( version.Major == 1 && version.Minor == 0 && version.Build == 2 && version.Revision == 0 );
            Assert.IsTrue( res );
        }

        [TestMethod]
        public void SecondNum_Test()
        {
            var version = new VersionExt( "1.0." );
            bool res = ( version.Major == 1 && version.Minor == 0 && version.Build == 0 && version.Revision == 0 );
            Assert.IsTrue( res );
        }

        [TestMethod]
        public void ZeroNum_Test()
        {
            try
            {
                var version = new VersionExt( "1." );
            }
            catch
            {
                Assert.IsTrue( true );
            }
        }

        #endregion

        #region Operators Test

        [TestMethod]
        public void Less_Test()
        {
            var v1 = new VersionExt( "1.0.3" );
            var v2 = new VersionExt( "1.0.3.01" );
            Assert.IsTrue( v1 < v2 );
        }

        [TestMethod]
        public void Larger_Test()
        {
            var v1 = new VersionExt( "5.34.30" );
            var v2 = new VersionExt( "01.00.3.01" );
            Assert.IsTrue( v1 > v2 );
        }

        [TestMethod]
        public void Equal_Test()
        {
            var v1 = new VersionExt( "5.34.30" );
            var v2 = new VersionExt( "05.034.0030" );
            Assert.IsTrue( v1 == v2 );
        }

        [TestMethod]
        public void NotEqual_Test()
        {
            var v1 = new VersionExt( "5.34.30" );
            var v2 = new VersionExt( "05.034.00301" );
            Assert.IsTrue( v1 != v2 );
        }

        #endregion
    }
}